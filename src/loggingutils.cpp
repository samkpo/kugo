#include <QtGlobal>
#include <QMessageLogger>
#include <QFile>
#include <QDir>
#include <QFileInfo>
#include <QDateTime>
#include <QTextStream>
#include <QDebug>
#include <iostream>
#include "loggingutils.h"

LoggingUtils::LoggingUtils()
{
    // Get home path
    QFileInfo file(QDir::homePath(), mLogFileName);
    qDebug() << file.absoluteFilePath();

    //Initiate the file
    mLoggingFile.setFileName(file.absoluteFilePath());

	//Open file. If cant then close
	if(!mLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append)){
		std::cerr << "Couldn't open file: \""
                  << file.absoluteFilePath().toStdString()
		          << "\""
		          << std::endl;

		abort();
	}

	//Set stream
	mTextStream.setDevice(&mLoggingFile);
}

void LoggingUtils::setUser(const QString user)
{
	this->mUserName = user;
}

LoggingUtils::~LoggingUtils()
{
	fprintf(stderr, "Closing file\n"); //Some info, for debugging sake..

	//Close file
	mLoggingFile.close();

}

void LoggingUtils::formatOutput(QString type)
{
	//The output message has the form:
	// [dd/MM/yy hh:mm:ss.zzz USERNAME TYPE]: 
	mTextStream << "["
                << QDateTime::currentDateTime().toString("dd/MM/yy hh:mm:ss.zzz");

    //No user -> no output to stream
    if(!mUserName.isEmpty())
    	mTextStream << " " << mUserName;

    //No type, no output to stream
    if(!type.isEmpty())
    	mTextStream << " " << type;

    //Close braces
    mTextStream << "]: ";
}

void LoggingUtils::logToFile(const QString msg, const QString type)
{
	//Format output
    this->formatOutput(type);

    //Put it in the stream
    mTextStream << msg << endl;
}

QString LoggingUtils::getLogFileName() const
{
	return mLogFileName;
}
