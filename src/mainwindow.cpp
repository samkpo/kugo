//Qt headers
#include <QMessageBox>
#include <QtGui>
#include <QTextStream>
#include <QTimer>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QSerialPort>
#include <QInputDialog>

//STL lib
#include <functional>
#include <iomanip>
#include <ostream>
#include <bitset>
#include <unistd.h>

//Own headers
#include "defines.h"
#include "mainwindow.h"
#include "aboutdialog.h"
#include "cargas.h"
#include "mname.h"
#include "connectdialog.h"
#include "comandos.h"
#include "cdebug.h"
#include "utils.h"

//Ui
#include "ui_mainwindow.h"

/**
 * @brief      Overload of operator<< for output with QDebug
 *
 * @param[in]  dbg   The debug
 * @param[in]  c     { parameter_description }
 *
 * @return     { description_of_the_return_value }
 */
std::ostream &operator<<(std::ostream &dbg, const Carga &c)
{
    //Output:
    //  [impedance Ω, potenciaLimite W_RMS, MSB:0b(MSB), LSB:0b(LSB)]
    //  qDebug("%s=%d", var1.toStdString().c_str(), var2);
    dbg << "("
            << std::setprecision(2) //<< forcepoint << fixed << right
            << std::setw(7) << c.impedancia     << std::setw(0) << " Ω, "
            << std::setw(7) << c.potenciaLimite << std::setw(0) << " W, "
            << std::setw(3) << std::to_string(c.MSB) << ":0b"   << std::bitset<8>(c.MSB).to_string().c_str()
            << ", " << std::to_string(c.LSB) << ":0b" << std::bitset<8>(c.LSB).to_string().c_str()
            << ")";

    return dbg;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
    ,ui(new Ui::MainWindow)
    ,m_corrienteExpression(new QRegularExpression(R"(cA(?<A>\d+)B(?<B>\d+)C(?<C>\d+) (?<D>\d+))")) // cA10007B0C0 53
    ,m_voltageExpression(new QRegularExpression(R"(vA(?<A>\d+)B(?<B>\d+)C(?<C>\d+) (?<D>\d+))")) //
    ,m_errorExpression(new QRegularExpression(R"(e(?<B>[A-C])(?<D>\d+)N(?<N>\d+) (?<C>\d+))")) // eA9247N132 114
    ,m_statusExpression(new QRegularExpression(R"(s(?<S>(CON|DIS))A(?<A>\d+)B(?<B>\d+)C(?<C>\d+) (?<D>\d+))")) // eA9247N132 114
{
#ifndef NDEBUG
    DEBUG << __METHOD_NAME__;
#endif

    //Let's configure the UI
    ui->setupUi(this);
    this->setWindowTitle(QLatin1String(PROJECT_LONGNAME)); //PROJECT_LONGNAME is defined in CMakeLists.txt

#if defined(Q_OS_WIN)
    QApplication::setStyle("fusion");
#endif

    //Configure th serial port
    mSerialPort = new QSerialPort();
    connect(mSerialPort, SIGNAL(readyRead()), this, SLOT(handleReadyRead()));

    //Configure loads and displays
    this->configureResistorsComboBox();
    this->configurarLCD();

    //Connect button
    connect(ui->sistemaEquilibradoCheckBox, SIGNAL(toggled(bool)), this, SLOT(cambiarEquilibradaNoEquilibradaSlot(bool)));
    connect(ui->ramaAComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(nuevoValorImpedanciaRamaA(int)));
    connect(ui->ramaBComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(nuevoValorImpedanciaRamaB(int)));
    connect(ui->ramaCComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(nuevoValorImpedanciaRamaC(int)));

    //Connect and disconnect action
    connect(ui->actionConectar_banco, SIGNAL(triggered()), this, SLOT(connectButtonSlot()));
    connect(ui->conectarCargaCheckBox, SIGNAL(toggled(bool)), this, SLOT(slotConectarRele(bool)));
    connect(ui->actionAcera_d, SIGNAL(triggered()), this, SLOT(aboutDialog()));
    connect(ui->actionSalir, SIGNAL(triggered()), this, SLOT(close()));

    connect(ui->rb380, SIGNAL(clicked()), this, SLOT(updateCombobox()));
    connect(ui->rb220, SIGNAL(clicked()), this, SLOT(updateCombobox()));
    connect(ui->rb127, SIGNAL(clicked()), this, SLOT(updateCombobox()));
    connect(ui->rbOther, SIGNAL(clicked()), this, SLOT(setOtherVoltage()));

    // settings
    this->configureSettings();
    this->updateCombobox();
}

void MainWindow::configureSettings()
{
    QCoreApplication::setOrganizationName("Kugo Team");
    QCoreApplication::setApplicationName("Kugo");
    QSettings settings;

#ifndef NDEBUG
    qDebug() << settings.fileName();
#endif

    // tiempo espera
    m_waitTime = settings.value("wait_time", 1000).toUInt();
    auto otherVolt = settings.value("other_voltage", 380).toDouble();
    this->setOtherText(otherVolt);
}

void MainWindow::handleReadyRead()
{
    QByteArray data;
    if ( mSerialPort->canReadLine()) {
        data = mSerialPort->readLine().trimmed();
#ifndef NDEBUG
        qDebug() << data;
#endif
    }

    if(not data.isEmpty())
        this->portNewData(data);
}

void MainWindow::configurarLCD()
{
#ifndef NDEBUG
    DEBUG << __METHOD_NAME__;
#endif

    //POWER
    mLcdPower[0] = ui->aPowerLCD;
    mLcdPower[1] = ui->bPowerLCD;
    mLcdPower[2] = ui->cPowerLCD;
    mLcdPower[3] = ui->totalPowerLCD;

    //Voltage
    mLcdVoltage[0] = ui->aVoltageLCD;
    mLcdVoltage[1] = ui->bVoltageLCD;
    mLcdVoltage[2] = ui->cVoltageLCD;

    //Current
    mLcdCurrent[0] = ui->aCurrentLCD;
    mLcdCurrent[1] = ui->bCurrentLCD;
    mLcdCurrent[2] = ui->cCurrentLCD;

    //Set autofill
    for(auto i : mLcdCurrent)
        i->setAutoFillBackground(true);
    for(auto i : mLcdPower)
        i->setAutoFillBackground(true);
    for(auto i : mLcdVoltage)
        i->setAutoFillBackground(true);

    //Normal
    this->normalLCD = ui->aPowerLCD->palette();
    this->highPowerLCD = ui->aPowerLCD->palette();
    this->normalLCD.setColor(QPalette::Normal, QPalette::Window, Qt::black);
    this->highPowerLCD.setColor(QPalette::Normal, QPalette::Window, Qt::black);
    this->normalLCD.setColor(QPalette::Normal, QPalette::WindowText, Qt::green);
    this->highPowerLCD.setColor(QPalette::Normal, QPalette::WindowText, Qt::red);

    /** \todo avoid blinking in the LDS's */

    //Displays
    //Set autofill
    for(auto i : mLcdCurrent)
        i->setPalette(this->normalLCD);
    for(auto i : mLcdPower)
        i->setPalette(this->normalLCD);
    for(auto i : mLcdVoltage)
        i->setPalette(this->normalLCD);
}

void MainWindow::requestStatus()
{
#ifndef NDEBUG
    qDebug() << "Request status";
#endif

    //Prepare data to send to bench
    uint8_t _data[] = {
            (uint8_t)mComandos.value(Comandos::Status).command, //Character
            0 //Value
    };

    //Send it
    this->mSerialPort->write((const char*)_data, 2);
}

void MainWindow::slotConectarRele(bool checked)
{
#ifndef NDEBUG
    DEBUG << __METHOD_NAME__;
#endif

    //Log info
    if(checked)
        WARNING << tr("Connecting main contactor");
    else
        WARNING << tr("Disconnecting main contactor");

    //Prepare data to send to bench
    uint8_t _data[] = {
        (uint8_t)mComandos.value(Comandos::Conectar).command, //Character
        (uint8_t)(checked ? 255 : 0)                          //Value
    };

    //Send it
    this->mSerialPort->write((const char*)_data, 2);

    //Disable the main contactor button for a while, so the user won't pay with it
    INFO << tr("Disabling main contactor button for %1 seconds").arg(m_waitTime);
    ui->conectarCargaCheckBox->setEnabled(false);
    QTimer::singleShot(m_waitTime * 2, this, SLOT(habilitarBotonConexionRele()));
}

void MainWindow::habilitarBotonConexionRele()
{
#ifndef NDEBUG
    DEBUG << __METHOD_NAME__;
#endif

    INFO << tr("Enabling main contactor button after delay");
    ui->conectarCargaCheckBox->setEnabled(true);
}

void MainWindow::cambiarEquilibradaNoEquilibradaSlot(bool b)
{
#ifndef NDEBUG
    DEBUG << __METHOD_NAME__;
#endif

    //Log it
    INFO << tr("Switching to %1 load").arg(b ? tr("balanced") : tr("unbalanced"));

    //If it's equilibrated, we set branches B and C with the value of branch A
    if(b){ //Equilibrated
        int i = ui->ramaAComboBox->currentIndex();
        ui->ramaBComboBox->setCurrentIndex(i);
        ui->ramaCComboBox->setCurrentIndex(i);

        //Equilibrated load, log new value
        INFO << tr("Setting loads to [%1]: ").arg(i) << mCargas[i].impedancia;

        //Data of equilibrated connection
        uint8_t _data[] = {
            (uint8_t)mComandos.value(Comandos::General).command,
            (uint8_t)((i+1) >> 8),  //MSB
            (uint8_t)((i+1) & 0xFF) //LSB
        };
        mSerialPort->write((const char*)_data, 3);

#ifndef NDEBUG
        DEBUG << __LINE__ << ": _data: " << _data << "value(Comandos::General): " << mComandos.value(Comandos::General).command;
#endif
    }
}

void MainWindow::setOtherText(double voltage)
{
    this->ui->rbOther->setText(tr("Otro: %1 volts").arg(voltage));
    this->ui->rbOther->setProperty("voltage", voltage);
}

double get_max_voltage()
{
    double max = 0.0;
    for(auto &c: mCargas){
        if (c.voltageLimite > max) {
            max = c.voltageLimite;
        }
    }

#ifndef DEBUG
    qDebug() << "Max voltage: " << newValue;
#endif

    return max;
}

void MainWindow::setOtherVoltage()
{
    auto newValue = QInputDialog::getDouble(
            this, "Nuevo limite",
            "Ingrese un nuevo voltage maximo",
            this->ui->rbOther->property("voltage").value<double>(),
            0,
            get_max_voltage()
    );

#ifndef DEBUG
    qDebug() << "New value: " << newValue;
#endif

    this->setOtherText(newValue);
    this->updateCombobox();
}

void MainWindow::updateCombobox()
{
    // the max voltage
    auto limit = std::max(std::begin(mCargas), std::end(mCargas),
        [](const auto& a, const auto& b) {
            return a->voltageLimite > b->voltageLimite;
        }
    )->voltageLimite;

    // 380
    if(ui->rb380->isChecked())
        limit = 380;
    else if (ui->rb220->isChecked())
        limit = 220;
    else if (ui->rb127->isChecked())
        limit = 127;
    else if (ui->rbOther->isChecked())
        limit = ui->rbOther->property("voltage").value<double>();

#ifndef NDEBUG
    qDebug() << "New volts: " << limit;
#endif
    INFO << "New voltage limit: " << limit;

    // iterate
    for(uint32_t i = 0; i < cantidad_de_resistencias; ++i){
        auto _max = mCargas[i].voltageLimite;
        for(auto cb: mListasImpedancias){
            auto model = qobject_cast<QStandardItemModel*>(cb->model());
            auto item = model->item(i);
            item->setEnabled(_max >= limit);
        }
    }

    // Take the resistors to the max values, and make the user choose again
    for(auto cb: mListasImpedancias)
        cb->setCurrentIndex(cantidad_de_resistencias - 1);
}

void MainWindow::configureResistorsComboBox()
{
#ifndef NDEBUG
    DEBUG << __METHOD_NAME__;

    DEBUG << "Configuring resistors. There are: "
          << cantidad_de_resistencias
          << " resistors";
#endif

    //This is useful for when we need to make general changes
    this->mListasImpedancias[0] = ui->ramaAComboBox;
    this->mListasImpedancias[1] = ui->ramaBComboBox;
    this->mListasImpedancias[2] = ui->ramaCComboBox;

    //Fill the lists
    for(QComboBox *q : mListasImpedancias){
        for(Carga c : mCargas)
            q->addItem(QString::number(c.impedancia));

        //Set the list to the last element
        q->setCurrentIndex(cantidad_de_resistencias - 1);
    }
}

void MainWindow::nuevoValorImpedanciaRamaA(int n)
{
    this->nuevasImpedancias(n+1, Rama::RamaA);
}

void MainWindow::nuevoValorImpedanciaRamaB(int n)
{
    if(!ui->sistemaEquilibradoCheckBox->isChecked())
        this->nuevasImpedancias(n+1, Rama::RamaB);
}

void MainWindow::nuevoValorImpedanciaRamaC(int n)
{
    if(!ui->sistemaEquilibradoCheckBox->isChecked())
        this->nuevasImpedancias(n+1, Rama::RamaC);
}

void MainWindow::habilitarListasImpedancias()
{
    INFO << tr("Enabling impedance lists.");

    //Enable all lists
    for(auto a : this->mListasImpedancias){
        //If it's not branch A, and the configuration is equilibrated, we exit
        if((a != ui->ramaAComboBox) && (ui->sistemaEquilibradoCheckBox->isChecked()))
            continue;

        a->setEnabled(true);
    }
}

std::ostream& operator<<(std::ostream& os, const MainWindow::Rama &r)
{
    switch(r){
        case MainWindow::RamaA: os << 'A'  ; break;
        case MainWindow::RamaB: os << 'B'  ; break;
        case MainWindow::RamaC: os << 'C'  ; break;
    }

    return os;
}

void MainWindow::nuevasImpedancias(int index, Rama rama)
{
    //Disable the lists for a while
    INFO << tr("Disabling impedance lists for %1 seconds").arg(m_waitTime);
    for(auto a : mListasImpedancias)
        a->setEnabled(false);

    //Enable them in m_waitTime seconds
    QTimer::singleShot(m_waitTime, this, SLOT(habilitarListasImpedancias()));

    //Check if the system is equilibrated before anything
    if(ui->sistemaEquilibradoCheckBox->isChecked()){
#ifndef NDEBUG
        DEBUG << tr("Equilibrated system");
#endif

        //Log it
        INFO << tr("All branches [")
             << std::setw(3) << index//fixed << right << index
             << "]: "
             << mCargas[index];

        //Send data, General for this case
        uint8_t _data[] = {
            (uint8_t)mComandos.value(Comandos::General).command,
            (uint8_t)(index >> 8),  //MSB
            (uint8_t)(index & 0xFF) //LSB
        };
        mSerialPort->write((const char*)_data, 3);

    } else {
        //Log it
        INFO << tr("Branch [") << rama << "]["
             << std::setw(3) << index//fixed << right << index
             << "]: "
             << mCargas[index];

        //Prepare data
        char a = 'x';
        switch(rama){
        case RamaA: a = mComandos.value(Comandos::RamaA).command; break;
        case RamaB: a = mComandos.value(Comandos::RamaB).command; break;
        case RamaC: a = mComandos.value(Comandos::RamaC).command; break;
        }

        uint8_t _data[] = {
            (uint8_t)a,
            (uint8_t)(index >> 8),  //MSB
            (uint8_t)(index & 0xFF) //LSB
        };
        mSerialPort->write((const char*)_data, 3);
    }

    //Set all the other branches UI in case it's branch A
    if(rama != RamaA)
        return;

    //If the system is equilibrates then we move all the combo boxes at once
    if(ui->sistemaEquilibradoCheckBox->isChecked()){
        INFO << tr("Equilibrated configuration. Setting UI to reflect it.");
        ui->ramaBComboBox->setCurrentIndex(index-1);
        ui->ramaCComboBox->setCurrentIndex(index-1);
    }
}

void MainWindow::portNewData(QString s)
{
    if(this->pPortOpen)
        this->procesarNuevaOrden(s);
    else {
        this->testNewConnection(s);
    }
}

void MainWindow::procesarNuevaOrden(QString s)
{
    //Aca suponemos que la orden llega formateada, es decir, comienza con alguno
    //de los codigos descriptos en el Enum \sOpciones
    if(s.isEmpty()){
#ifndef NDEBUG
        ERROR << "Empty data from LPC.";
#endif
        return;
    }

    //Get first char, so we know what to process
    char opcion = s.at(0).toLower().toLatin1(); //Solo minusculas
#ifndef NDEBUG
    DEBUG << __FUNCTION__ << ": " << s << " :: " << opcion;
#endif

    //Process the data
    switch(opcion){
    case Corriente:
        this->procesarLectura(s, (Opciones)opcion, m_corrienteExpression);
        break;
    case Voltaje:
        this->procesarLectura(s, (Opciones)opcion, m_voltageExpression);
        break;
    case Error:
        WARNING << tr("Error sent from bench");
        this->procesarError(s, m_errorExpression);
        break;
    case Status:
        this->processStatus(s, m_statusExpression);
        break;
    default:
        ERROR << tr("Unknown code: %1").arg(s);
        break;
    }
}

void MainWindow::processStatus(QString str, QRegularExpression* exp)
{
    // Expression
    m_expressionMatch = exp->match(str); // s(?<S>(CON|DIS))A(?<A>\d+)B(?<B>\d+)C(?<C>\d+) (?<D>\d+)
    if(not m_expressionMatch.hasMatch()){
        WARNING << tr("Data reg expression didn't matched any data from bench");
        return; /// \TODO something more
    }
    // Check timer
//    if(not m_timer->isActive())
//        return;

    // Status
    auto status = m_expressionMatch.captured("S");
    auto bA = m_expressionMatch.captured("A").toInt();
    auto bB = m_expressionMatch.captured("B").toInt();
    auto bC = m_expressionMatch.captured("C").toInt();
    auto bD = m_expressionMatch.captured("D").toInt();

    // Check
    if(not this->checkString(str, bD)){
        ERROR << "Bad check sum";
        return;
    }

    // Update status
    if(status == "CON")
        this->ui->conectarCargaCheckBox->setChecked(true);
    else if(status == "DIS")
        this->ui->conectarCargaCheckBox->setChecked(false);
    else
        ERROR << "Wrong command";

    // set branches
    this->ui->ramaAComboBox->setCurrentIndex(bA);
    this->ui->ramaBComboBox->setCurrentIndex(bB);
    this->ui->ramaCComboBox->setCurrentIndex(bC);

    this->ui->sistemaEquilibradoCheckBox->setChecked((bA == bB && bB == bC));
}

/**
 * The error format is
 *      e(A|B|C)dd d
 *
 * @param lectura
 * @param regexp
 */
void MainWindow::procesarError(QString lectura, QRegularExpression *regexp)
{
    // Check the expression. e(?<B>[A-C])(?<D>\d+)N(?<N>\d+) (?<C>\d+)
    m_expressionMatch = regexp->match(lectura);
    if(not m_expressionMatch.hasMatch()){
        WARNING << tr("Data reg expression didn't matched any data from bench");
        return; /// \TODO something more
    }

    // we had a match
    char branch = m_expressionMatch.captured("B")[0].toLatin1();
    long currentDiff = m_expressionMatch.captured("D").toLong();
    long times = m_expressionMatch.captured("N").toLong();
    long check = m_expressionMatch.captured("C").toLong();

    // check
    if(not this->checkString(lectura, check)){
        ERROR << "Check sum wrong";
    }

    // Log data
    auto index = this->mListasImpedancias['C'-branch]->currentIndex();
    auto max_current = mCargas[index].MaxCurrent();
    ERROR << tr("Over current in branch %1 by %2 mA: max = %3 mA").arg(branch).arg(currentDiff).arg(max_current);
    if(m_errorDialogUp)
        return;

    // Error dialog
    m_errorDialogUp = true;
    auto _branch = (char)(branch + 'U' - 'A');
    QMessageBox::warning(
            this,
            tr("Over current"),
            tr("Over current in branch %1. Check the connections and reconnect.").arg(_branch),
            QMessageBox::StandardButton ::Ok);

    // Flush port
    this->mSerialPort->clear();

    // flag
    m_errorDialogUp = false;

    // Clear gui
    for(auto i: mListasImpedancias)
        i->setCurrentIndex(cantidad_de_resistencias-1);
    ui->sistemaEquilibradoCheckBox->setChecked(true);
    ui->conectarCargaCheckBox->setChecked(false);
    // disconnect
    this->connectButtonSlot();
}

void MainWindow::procesarLectura(QString lectura, Opciones op, QRegularExpression *regexp)
{
    //Check that it has the three branches data
    if(!lectura.contains('A') || !lectura.contains('B') || !lectura.contains('C')){
        WARNING << tr("Data doesn't have some of the three branches.");

        //! \todo mostrar dialogo
        return;
    }

    //Get values from string. In theory, they are in order. A->B->C.
    //Let's use a regular expression to get those values
    m_expressionMatch = regexp->match(lectura);
    if(m_expressionMatch.hasMatch()){
        int vec[] = {
            m_expressionMatch.captured("A").toInt(),
            m_expressionMatch.captured("B").toInt(),
            m_expressionMatch.captured("C").toInt()
        };

        //Data check
        this->checkSafeWord(lectura);

        //Update reading
        switch(op){
        case Corriente:
            this->setCurrentsRead(vec); //Update ui
            break;
        case Voltaje:
            this->setVoltagesRead(vec); //Update ui
            break;
        case Error:
            QMessageBox::warning(
                    this, "Error",
                    "Closing program, command received isn't valid.\nCall admin.",
                    QMessageBox::StandardButton::Ok);
            //exit(1);
        }
    } else
        WARNING << tr("ERROR while processing %1, no matches").arg(lectura);
}

bool MainWindow::checkString(QString str, uint32_t value)
{
    //Create a string from the lecture
    QString tempCheckString(str);

    //Remove the last number, and the space
    tempCheckString.remove(QRegularExpression("\\d+$")); //"cAxxxBxxxCxxxx yy" -> "cAxxxBxxxCxxxx "

    //check the data
    int temp_var = 0;
    for(char a : tempCheckString.toStdString()){
        temp_var ^= a;
    }

    return value == temp_var;
}

void MainWindow::checkSafeWord(QString lecture)
{
    //Get the check word (captured in the regular expression, it's the last byte)
    int checkByte = m_expressionMatch.captured("D").toInt();

    //Create a string from the lecture
    QString tempCheckString(lecture);

    //Remove the last number, and the space
    tempCheckString.remove(QRegularExpression("\\d+$")); //"cAxxxBxxxCxxxx yy" -> "cAxxxBxxxCxxxx "

    //check the data
    int temp_var = 0;
    for(char a : tempCheckString.toStdString()){
        temp_var ^= a;
    }

    //Send check word back to the bench
    uint8_t data[] = {
        (uint8_t)'k',
        (uint8_t)temp_var
    };
    mSerialPort->write((const char*)data, 2);

    //Check if we are  OK
    if(temp_var != checkByte){
        mErrorCounter++;
        WARNING << tr("Lecture %1 is wrong. It's number %2").arg(lecture).arg(mErrorCounter);
    } else {
        mErrorCounter = 0;
    }

    if(checkByte != temp_var) {
        ++this->mErrorCounter;
        if(this->mErrorCounter >= 10){
            ERROR << tr("Error check was wrong %1 times. Closing bench.").arg(this->mErrorCounter);
        }
    } else if (this->mErrorCounter > 0){
        --this->mErrorCounter;
    }
}

void MainWindow::setCurrentsRead(int v[3])
{
    //Temporary variable for calculations
    double _totalPower = 0;
    bool _overPower = false;

    //Do it for the three branches
    for(int i = 0; i < 3; i++){
        // current
        QString _currentStr = QString("%1").arg(v[i] * multiplicadorCorriente, 0, 'f', 2);
        double _maxPo = mCargas[mListasImpedancias[i]->currentIndex()].potenciaLimite;
        double _currRes = mCargas[mListasImpedancias[i]->currentIndex()].impedancia;
        double _current = _currentStr.toDouble();
        double _voltage = _current * _currRes;
        double _power   = _currRes * pow(_current, 2);

        //Currents
        mLcdCurrent[i]->display(_currentStr);

        //Voltages
        mLcdVoltage[i]->display(QString("%1").arg(_voltage, 0, 'f', 2));

        //Power (P = I^2 * R)
        mLcdPower[i]->display(QString("%1").arg(_power, 0, 'f', 2));
        if(_power >= _maxPo) {
            mLcdPower[i]->setPalette(this->highPowerLCD);
            _overPower = true; // only set
        } else {
            mLcdPower[i]->setPalette(this->normalLCD);
        }

        // total power
        _totalPower += _power;
    }

    // max power
    this->mLcdPower[3]->display(QString("%1").arg(_totalPower, 0, 'f', 2));
    if(_overPower)
        mLcdPower[3]->setPalette(this->highPowerLCD);
    else
        mLcdPower[3]->setPalette(this->normalLCD);
}

void MainWindow::setVoltagesRead(int v[3])
{
    //Iterate over the three branches
    for(int i = 0; i < 3; i++){
        //Voltage
        mLcdVoltage[i]->display(v[i] * multiplicadorVoltage);

        //Current
        mLcdCurrent[i]->display(v[i] *  multiplicadorVoltage / mCargas[mListasImpedancias[i]->currentIndex()].impedancia);
    }
}

void MainWindow::testNewConnection(QString response)
{
    // counter
    //static int counter = 0;

    //Not the bench
    QString conn_word = QString("%1_OK").arg(CONNECTION_WORD);
#ifndef NDEBUG
    qDebug() << "Connection word:\n\t" << conn_word;
    qDebug() << "Response:\n\t" << response;
#endif

    //Test it
    bool is_ok = (response.compare(conn_word) == 0);
    if(!is_ok){
        ERROR << tr("It isn't the power bench. Try with another port,"
                    " or disconnecting everything else.");

        //Disconnect from port
        mSerialPort->close();

        //The bench isn't the one connected to the port
        QMessageBox::warning(this,
                             tr(PROJECT_NAME),
                             is_ok ? tr("The port is connected to the bench, going on.")
                                   : tr("It isn't the power bench. Try with another port,"
                                        " or disconnecting everything else."),
                             QMessageBox::Ok);
    } else {
        INFO << tr("The port is connected to the bench, going on.");

        //Let the app know that the port is open
        this->pPortOpen = true;

        //Enable the buttons that control the bench
        this->connectedButtons(true);

        //Change connect button status
        ui->actionConectar_banco->setText(tr("Disconnect"));
    }

}

void MainWindow::connectButtonSlot()
{
#ifndef NDEBUG
    DEBUG << __METHOD_NAME__;
#endif

    //Reflect it in the button
    ui->actionConectar_banco->setText(
        pPortOpen ? tr("Disconnecting")
                  : tr("Connecting"));

    //Connect or disconnect
    if(pPortOpen){ //Disconnect
        INFO << tr("Closing serial port");

        //Data
        uint8_t _data[] = {
            (uint8_t)mComandos.value(Comandos::Conectar).command,
            (uint8_t)0
        };

        // Send the data
        mSerialPort->write((const char*)_data, 2);

        // Close port
        mSerialPort->close();

        //Open the main contactor on disconnecting
        ui->conectarCargaCheckBox->setChecked(false);

        //Disable buttons
        this->connectedButtons(false);

        //Button
        ui->actionConectar_banco->setText(tr("Connect"));

        // Disable flag
        pPortOpen = false;
    } else { //Connect to port
        INFO << tr("Opening connect dialog");

        //Create and show dialog
        ConnectDialog aDialog(this);
        connect(&aDialog, SIGNAL(portOpened(bool)), this, SLOT(connectDialogSlot(bool)));
        aDialog.setPortToConnect(this->mSerialPort);
        aDialog.exec();

        //We'll enable the (dis)connect button in the check connection slot
    }
}

/**
 * \todo send it to the makefile
 */
void MainWindow::connectDialogSlot(bool opened)
{
    if(opened){
        //Log
        INFO << tr("Serial port connected");

        //Check if it's the power bench, by calling the method \a testNewConnection
        QTimer::singleShot(2100, this, [&](){
            this->mSerialPort->clear();
            this->mSerialPort->write(CONNECTION_WORD);
        });

        //Log that we are connected
        WARNING << tr("The bench is CONNECTED");
    } else {
        INFO << tr("Unable to connect to the bench.");

        //Butrton
        ui->actionConectar_banco->setText(tr("Connect"));
    }

    /**
     * Now on the slot \a testNewConnection the device on the port will be
     * tested, and if it is the power bench it'll activate all the buttons
     * and stuff, and if not it'll show a message box informing the user.
     */
}

void MainWindow::connectedButtons(bool connected)
{
    //If the system is connected we enable the widgets that are used
    ui->disconnectAction->setEnabled(connected);
    ui->actionCDialog->setEnabled(!connected);

    ui->banchsDockWidget->setEnabled(connected);

    ui->conectarCargaCheckBox->setEnabled(connected);
    ui->totalPowerGB->setEnabled(connected);
    ui->conectionTypeGB->setEnabled(connected);
    ui->imageLabel->setEnabled(connected);
}

void MainWindow::aboutDialog()
{
    AboutDialog aDialog(this);
    aDialog.exec();
}

MainWindow::~MainWindow()
{
    delete m_voltageExpression;
    delete m_corrienteExpression;
    delete m_errorExpression;
    delete mSerialPort;
    delete ui;
}

