#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPalette>
#include <QRegularExpressionMatch>
#include "cargas.h"

namespace Ui {
class MainWindow;
}

class QSerialPort;
class QLabel;
class QComboBox;
class QLCDNumber;
class QSerialPort;

/**
 * @brief      Class for main window.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    /*!
     * \brief Options the app is going to read
     * 
     * This options comes from the LPCXpresso.
     */
    enum Opciones{
        Corriente = 'c', ///< States that the data is the branch current
        Voltaje = 'v',   ///< The reading is the branch voltage
        Error = 'e',      ///< There has been an error
        Status = 's'
    };

    /**
     * @brief      Bench branch being read
     */
    enum Rama{
        RamaA,  ///< Branch A
        RamaB,  ///< Branch B
        RamaC   ///< Branch C
    };

private:
    //Ventana principal
    Ui::MainWindow *ui;

    /**
     * List of loads (impedances), availables to set
     */
    QComboBox *mListasImpedancias[3];

    /**
     * LCD power widgets. 3, one for each branch.
     */
    QLCDNumber *mLcdPower[4];

    /**
     * LCD current widgets. 3, one for each branch.
     */
    QLCDNumber *mLcdCurrent[3];

    /**
     * LCD voltage widgets. 3, one for each branch.
     */
    QLCDNumber *mLcdVoltage[3];

    /**
     * This is a counter that will trigger an alarm if is bigger than the limit
     */
    int mErrorCounter = 0;

    qint64  m_waitTime = 5000;

    void configureSettings();

    /**
     * @brief      (De)Activates the app buttons
     * 
     * This enabling/disabling depends on weather the app is connected or not
     * to the LPC
     */
    void connectedButtons(bool);

    /*!
     * @brief      Configures the resistors combo lists
     *
     * This method is called just once, at the program startup, to fullfill
     * the resistors lists, and set them to the latest, the bigger (in ohms)
     * speaking.
     */
    void configureResistorsComboBox();

    /**
     * Reagular expresions that will parse the data received from the bench,
     * so we can show those values on the displays (LCD widgets)
     */
    QRegularExpression *m_corrienteExpression;
    QRegularExpression *m_voltageExpression;
    QRegularExpression *m_errorExpression;
    QRegularExpression *m_statusExpression;
    QRegularExpressionMatch m_expressionMatch;

    /**
     * A palette for the LCD widgets when they are ok (no over current/voltage)
     */
    QPalette normalLCD;

    /**
     * A palette fro the LCD widgets when they are in distress (over 
     * current/voltage)
     */
    QPalette highPowerLCD;
    void configurarLCD();

    //Variable para saber si el puerto esta abierto
    bool pPortOpen = false;

    //Serial port
    QSerialPort  *mSerialPort;

    // Error dialog on
    bool m_errorDialogUp = false;

    /*!
     * \brief Process the arraiing data from the LPC
     *
     * Cada nueva orden que llega de la placa es procesada por esta funcion, y
     * luego setea los valores de los displays
     */
    void procesarNuevaOrden(QString);

    /**
     * @brief      Process the readings, and sets the displays
     *
     * @param[in]  lectura  The lectura
     * @param[in]  op       The operation
     * @param      regexp   The regular expression
     */
    void procesarLectura(QString lectura, Opciones op, QRegularExpression *regexp);
    void procesarError(QString lectura, QRegularExpression *regexp);
    void setCurrentsRead(int v[3]);
    void setVoltagesRead(int v[3]);

    /*!
     * @brief      nuevasImpedancias Este metodo es llamado en cada
     *             actualizacion de la seleccion de impedancias
     *
     *             El usuario, al seleccionar una impedancia de las listas, cada
     *             una de las ramas tienen su propio Qt Slot. Luego estos slots
     *             invocan a este metodo, el cual decide que comando enviar a la
     *             LPC, dependiendo de si es una carga equilibrada o no.
     * @see        nuevoValorImpedanciaRamaA()
     * @see        nuevoValorImpedanciaRamaB()
     * @see        nuevoValorImpedanciaRamaC()
     *
     * @param[in]  <unnamed>  { parameter_description }
     */
    void nuevasImpedancias(int, Rama);

    /**
     * @brief      Check whether the reading matches the checking word
     * 
     * If the reading doesn't check then it'll start a counter, and after the 
     * counter reaches a predefined number \a errorCounter it'll stop the
     * program execution.
     *
     * @param[in]  lecture  The current lecture
     */
    void checkSafeWord(QString lecture);

    bool checkString(QString str, uint32_t value);

    void processStatus(QString str, QRegularExpression* exp);

    void setOtherText(double voltage);

private slots:
    void aboutDialog();
    void connectDialogSlot(bool);
    void cambiarEquilibradaNoEquilibradaSlot(bool);
    void portNewData(QString);

    void handleReadyRead();

    void requestStatus();

    void updateCombobox();
    void setOtherVoltage();

    /**
     * @brief      Slotto handle the connection dialog
     */
    void connectButtonSlot();

    /**
     * @brief      this slot will be called after a certain delay (used to give
     *             some time to settle to the bench) and then send the test
     *             keyword
     */
    void testNewConnection(QString);

    /*!
     * \brief slotConectarRele
     *
     * Este metodo es llamada cada vezs que se oprime el boton correspondiente,
     * enviando la señal de conectar/desconectar
     */
    void slotConectarRele(bool checked);

    /*!
     * \brief habilitarListasImpedancias
     *
     * Este metodo es llamado para habilitar los "ComboBox" de las impendacias
     * una vez que fueron deshabilitadas por el tiempo de seguridad
     */
    void habilitarListasImpedancias();
    void habilitarBotonConexionRele();

    /*!
     * \brief nuevoValorImpedanciaRamaA
     *        Metodo encargado de setear el valor de la rama A
     */
    void nuevoValorImpedanciaRamaA(int);

    /*!
     * \brief nuevoValorImpedanciaRamaB
     *        Metodo encargado de setear el valor de la rama B
     */
    void nuevoValorImpedanciaRamaB(int);

    /*!
     * \brief nuevoValorImpedanciaRamaC
     *        Metodo encargado de setear el valor de la rama C
     */
    void nuevoValorImpedanciaRamaC(int);

};

#endif // MAINWINDOW_H
