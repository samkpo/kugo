#ifndef GLOBAL_DEFINES_H
#define GLOBAL_DEFINES_H

// An anonymous namespace restricts these variables to the scope of the
// compilation unit.
namespace {
    const char* PROJECT_NAME = "@PROJECT_NAME@";
    const char* PROJECT_LONGNAME = "@PROJECT_LONGNAME@";
    const char* PROJECT_VERSION = "@PROJECT_VERSION@";
    const char* PROJECT_ORGANIZATION = "@PROJECT_ORGANIZATION@";
    const char* GIT_LONG_VERSION = "@GIT_LONG_VERSION@";
    const char* ICON_THEMES_DEFAULT_FOLDER = "@ICON_THEMES_DEFAULT_FOLDER@";
    const char* DATADIR = "@DATADIR@";
    const char* PKGDATADIR = "@PKGDATADIR@";
    const char* CONNECTION_WORD = "@CONNECTION_WORD@";

    //Valores obtenidos por calculo, para hacer el procesamiento aca y no en la placa
    const double multiplicadorVoltage = ${VOLTAGE_MULTIPLIER};
    const double multiplicadorCorriente = ${CURRENT_MULTIPLIER};
}

#endif
// vim: ft=cpp
