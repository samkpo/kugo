#ifndef LOGGING_UTILS_H
#define LOGGING_UTILS_H

#include <QString>
#include <QTextStream>
#include <QFile>

/*!
 * \class LoggingUtils
 * \brief Used to log data into a file.
 * 
 * This class provides some abstraction to the task of recording everything
 * into a file, for later analysis.
 * 
 * To log some data into the file, the user must get the instance and then
 * can call the method @see logToFile.
 * Also the class provides an option to manipulate it's input by using the
 * overloaded \see operator<<, and combined with the definition above one
 * can call:
 * 
 * \code
 * LoggingUtils::getInstance() << "Message";
 * \endcode
 * 
 */
class LoggingUtils
{
public:
	/*!
	 * Singleton
	 * 
	 * We are going to use a singleton here because we want just one object to
	 * be poking around the logging file.
	 * To access that object we just call \a getInstance:
	 * 
	 * \code
	 * LoggingUtils::getInstance().logToFile(msg, type)
	 * \endcode
	 */
	static LoggingUtils& getInstance()
    {
        static LoggingUtils instance; // Guaranteed to be destroyed.
                                      // Instantiated on first use.
        return instance;
    }

    /*!
     * This returns the logging file name.
     */
    QString getLogFileName() const;

    /*!
     * Prints \a msg in the opened file
     */
    void logToFile(const QString msg, const QString type = "");
    void logToFile(const std::string msg, const QString type = ""){
    	logToFile(QString::fromStdString(msg), type);
    }

    /*!
     * Sets the current user (to blame >:-) )
     */
    void setUser(const QString userName);

    /*!
     * Whit this operator you send the message that you want to show.
     */
    template <class T>
    LoggingUtils &operator<<(const T &t){
    	formatOutput();
    	mTextStream << t << endl;
        
        return *this;
    };
    

private:
	/*!
	 * Constructor
	 * It's private so only one instance of this class is initialized
	 */
	LoggingUtils();

	~LoggingUtils();

	/*!
	 * Declare this methods so we don't get copies of the instance
	 */
    LoggingUtils(LoggingUtils const&)    = delete;
    void operator=(LoggingUtils const&)  = delete;

    /*!
     * Store the name of the logging file
     * \todo {Move the file name to one defined by compile time}
     */
    QString mLogFileName = "kugo_log.txt";

	/*!
	 * Stores the user name that will be logged
	 */
	QString mUserName;

	/*!
	 * The file where we are shooting all the messages
	 */
	QFile mLoggingFile;
	QTextStream mTextStream;

	/*!
	 * Prints the current time/date in the output, along with the user
	 * name, in case it was previously defined, and \a type is the the
	 * output message type (ERROR; LOG; INFO..), also defined by the
	 * user, or left empty.
	 */
	void formatOutput(QString type="");

};

#endif
