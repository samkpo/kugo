###############################################################################
################################## Libraries ##################################
###############################################################################
ADD_DEFINITIONS(${QT_DEFINITIONS})
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../)

#This takes the defines.h.cmake file, and completes it with comilations data
#setted when calling cmake
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/defines.h.cmake
               ${CMAKE_CURRENT_BINARY_DIR}/defines.h)

#Archivos de diseño de la aplicacion
file(GLOB_RECURSE UI_FILES forms/*.ui)
file(GLOB_RECURSE CODE_FILES *.cpp)

#Recursps Qt
qt5_wrap_ui(UI_HEADERS ${UI_FILES})
qt5_add_resources(RESOURCE_FILES resources/kugo.qrc)
find_package(Qt5Core REQUIRED)

if (WIN32)
    #Creamos ejecutable
    add_executable(${CMAKE_PROJECT_NAME} WIN32
            ${CODE_FILES}
            ${RESOURCE_FILES}
            ${UI_HEADERS}
            )

    message("-- On windows")
else()
    #Creamos ejecutable
    message("-- Not on windows")
    add_executable(${CMAKE_PROJECT_NAME}
      ${CODE_FILES}
      ${RESOURCE_FILES}
      ${UI_HEADERS}
    )
endif()

qt5_use_modules(${CMAKE_PROJECT_NAME} Core SerialPort)
target_link_libraries(${CMAKE_PROJECT_NAME}
  Qt5::Widgets
)

###############################################################################
################################# Doxygen doc #################################
###############################################################################
find_package(Doxygen)
option(BUILD_DOCUMENTATION "Create and install the HTML based API documentation (requires Doxygen)" ${DOXYGEN_FOUND})

if(BUILD_DOCUMENTATION)
    if(NOT DOXYGEN_FOUND)
        message(FATAL_ERROR "Doxygen is needed to build the documentation.")
    endif()


    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in
                   ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)

    add_custom_target(doc
                  ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
                  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                  COMMENT "Generating API documentation with Doxygen" VERBATIM)

    install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/html DESTINATION share/doc)
endif()

###############################################################################
################################ Install files ################################
###############################################################################
#Icnono para lanzar la aplicacion en el sistema
#SET(DESKTOP_FILE ../resources/kugo.desktop)
#INSTALL (FILES ../resources/kugo.png DESTINATION ${DDM_PIXMAPS_DIR})
INSTALL(TARGETS ${CMAKE_PROJECT_NAME}
        RUNTIME
        DESTINATION bin
        PERMISSIONS OWNER_READ OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
INSTALL(FILES ${qm_files}
        DESTINATION ${PKGDATADIR}/locale
        PERMISSIONS OWNER_READ GROUP_READ WORLD_READ)
INSTALL(PROGRAMS ${DESKTOP_FILE} DESTINATION ${XDG_APPS_INSTALL_DIR} )
SET(DDM_PIXMAPS_DIR     "/usr/share/icons/")
