#include <QtGui>
#include <QApplication>
#include <QDateTime>
#include <getopt.h>
#include <iostream>
#include "defines.h"
#include "mainwindow.h"
#include "cdebug.h"

int main(int argc, char* argv[])
{
#ifndef NDEBUG
    std::cout << "Logging fFile: " << LoggingUtils::getInstance().getLogFileName().toStdString() << std::endl;
#endif

    //Log that we are starting the app
    INFO << "=========================== Application Started ===============================";

    //Qt Application
    QApplication app(argc, argv);

    //all the translation stuff was taken from minitube
    const QString locale = QLocale::system().name();

    //Mostramos algunos datos
#ifndef NDEBUG
    DEBUG << "Locale: " << locale;
    DEBUG << "Multiplicador voltaje: " << multiplicadorVoltage;
    DEBUG << "Multiplicador corriente: " << multiplicadorCorriente;
    DEBUG << "Version: " << PROJECT_VERSION;
    DEBUG << "Project name: " << PROJECT_NAME;
    DEBUG << "Project organization: " << PROJECT_ORGANIZATION;
    DEBUG << "Git commit: " << GIT_LONG_VERSION;
    DEBUG << "Date time: " << __DATE__ << __TIME__;
    DEBUG << "Connection word:" << CONNECTION_WORD;
#endif

    // qt translations
    QTranslator qtTranslator;
    qtTranslator.load("qt_" + locale, QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    // app translations
    QString dataDir = QLatin1String(PKGDATADIR);
//#ifndef NDEBUG
//    qDebug() << __METHOD_NAME__ << "Data dir: " << __FUNCTION__ << dataDir;
//#endif
  
//#ifdef USE_DEVELOPING
//    qDebug() << __METHOD_NAME__ << "Developing";
//#else
//    qDebug() << __METHOD_NAME__ << "Not developing";

#if defined(Q_OS_OS2) //|| defined(Q_OS_WIN) ->this isn't checked
    QString    localeDir = qApp->applicationDirPath() + QDir::separator() + "locale";
#else
    QString    localeDir = dataDir + QDir::separator() + "locale";
#endif

    //qDebug() << __METHOD_NAME__ << "Locale dir: " << localeDir;
    QTranslator translator;
    translator.load(locale, localeDir);
    app.installTranslator(&translator);

    //App main window
    MainWindow w;
    w.show();

    return app.exec();
}
