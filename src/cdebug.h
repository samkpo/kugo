#ifndef CDEBUG_H
#define CDEBUG_H

#include <QTextStream>
#include <QString>
#include <sstream>
#include "loggingutils.h"

/* \TODO add some colors */
/* \TODO add LOG option */

/*! @brief INFO logging macro. */

#define INFO    CDebug(CDebug::info,__FUNCTION__,__LINE__)
#define DEBUG   CDebug(CDebug::debug,__FUNCTION__,__LINE__)
#define WARNING CDebug(CDebug::warning,__FUNCTION__,__LINE__)
#define ERROR   CDebug(CDebug::error,__FUNCTION__,__LINE__)
#define READING CDebug(CDebug::reading,__FUNCTION__,__LINE__)

/**
 * @brief      Class for debug.
 * 
 * This class provides some utilities for putting debugging and info messages
 * in a logging file, within four levels:
 * 
 * 	- DEBUG
 * 	- INFO
 * 	- WARNING
 * 	- ERROR
 * 	- READING. Only for values read from the bench
 * 	
 * All of them are just tags that are attached on the logging message, that is
 * formatted as follows:
 * 
 * ```
 * [DD/MM/YY hh:mm:ss.zzz USER LABEL]: message
 * ```
 * 
 * The use of this class is quite simple. For any of the four levels mentioned
 * above, there's a macro that has the same name. for instance, to log an info
 * message:
 * 
 * \code
 * INFO << "The potatoes aren't ready yet";
 * \endcode
 * 
 * That produces the following output:
 * 
 * ```
 * [29/12/16 12:00:03.000 fulano INFO]: The potatoes aren't ready yet
 * ```
 * 
 * 
 * \todo rename the class to reflect the actual use
 */
class CDebug
{
public:
	/**
	 * @brief      Specifies the type of output to show
	 * 
	 * Each of them defines a logging level
	 */
	enum Type{
		info,
		debug,
		warning,
		reading,
		error
	};

    /*!
     * Constructor
     * According to \a t, the output will be formatted.
     * if \a t is \a CDebug::Debug, then in the output it will
     * show \a funcName, assuming that this is the name of the function
     * where it's called, and if \a line is >= 0, then this will be used
     * as the line number to show.
     */
    CDebug(Type t, const QString &funcName, int line = -1){
    	init(t, funcName, line, _debug);
    }

    /*!
     * With this operator you send the message that you wanna show.
     */
    template <class T>
	CDebug &operator<<(const T &t){
		_oss << t;

		return *this;
	};

	/**
	 * QString stuff
	 */
	CDebug &operator<<(const QString &str){
		_oss << str.toStdString(); 
		
		return *this;
	};

	/**
	 * @brief      Before destroying the object, we flush the data
	 * to the logger
	 */
	~CDebug();

private:
	/**
	 * @brief      Init the debugging output
	 *
	 * @param[in]  t         type of debugging to do
	 * @param[in]  funcName  The function name
	 * @param[in]  line      The line
	 * @param[in]  d         is debugging or not
	 * 
	 * \todo check the funcName
	 */
	void init(Type t, const QString &funcName, int line, bool d);

	/**
	 * Type of debugging desired
	 */
	Type _type;

	/**
	 * Caller function name
	 */
	QString pFuncName;

	/**
	 * Stream where we put the data
	 */
	std::ostringstream _oss;

#ifdef NDEBUG
	const bool _debug = false;
#else
	const bool _debug = true;
#endif
};

#endif
