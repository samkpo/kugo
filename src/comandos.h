#ifndef COMANDOS_H
#define COMANDOS_H

#include <QMap>

/**
 * This enum contains all the available commands that the Bench knows
 */
enum class Comandos{
    Conectar,
    TipoConexion,
    Manejo,
    General,
    RamaA,
    RamaB,
    RamaC,
    Status
};

/**
 * @brief      Definition of commands. Each command consists of it's @a name,
 *             and the char @a command reprenting it.
 */
struct Command{
    QString name; ///< Name, for user easy reading
    char command; ///< Char, that will be sent to the bench
};

/**
 * A map that links each command from \see Comandos to the corresponging struct
 */
QMap<Comandos, Command> mComandos = {
    {Comandos::Conectar,     {"conectar", 'c'}},
    {Comandos::TipoConexion, {"tipo_conexion", 't'}},
    {Comandos::Manejo,       {"manejo", 'm'}},
    {Comandos::General,      {"general", 'g'}},
    {Comandos::RamaA,        {"rama_a", 'x'}},
    {Comandos::RamaB,        {"rama_b", 'y'}},
    {Comandos::RamaC,        {"rama_c", 'z'}},
    {Comandos::Status,       {"status", 's'}}
};

#endif // COMANDOS_H
