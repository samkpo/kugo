#include <QtGui>
#include "aboutdialog.h"
#include "defines.h"
#include "utils.h"

AboutDialog::AboutDialog(QWidget* parent, Qt::WindowFlags fl) : QDialog( parent, fl )
{
    ui.setupUi(this);

    //Version de la aplicacion
#ifndef NDEBUG
    ui.versionLabel->setText(tr("Version %1").arg(QLatin1String(GIT_LONG_VERSION)));
#else
    ui.versionLabel->setText(tr("Version %1").arg(QLatin1String(PROJECT_VERSION)));
#endif
    
    //Descripcion
    ui.descriptionLabel->setText(Utils::stringFromFile(":descripcion.txt"));

    //Autores
    ui.authorLabel->setText(Utils::stringFromFile(":autores.txt"));

    //Agradecimientos
    ui.thaksToLabel->setText(Utils::stringFromFile(":agradecimientos.txt"));

    //Licencias
    ui.licenseText->setText(Utils::stringFromFile(":license.txt"));

    //Ok button
    ui.okButton->setIcon(QIcon::fromTheme("dialog-ok"));

    this->resize(QSize(470, 270));
}
