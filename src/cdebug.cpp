#include "cdebug.h"

void CDebug::init(Type t, const QString &funcName, int line, bool _debug)
{
	//Save variable
	_type = t;

	//Function name
	pFuncName = funcName;

	//_debug == false : no debug messages wanted
	if(!_debug && (t == debug)){
		return;
	}
}

CDebug::~CDebug(){
	switch (_type) {
	case debug:
		fprintf(stderr, "%s\n", _oss.str().c_str());
		break;
	case reading:
		LoggingUtils::getInstance().logToFile(_oss.str(), "READING");
		break;
	case info:
		LoggingUtils::getInstance().logToFile(_oss.str(), "INFO");
		break;
	case warning:
		LoggingUtils::getInstance().logToFile(_oss.str(), "WARNING");
		break;
	case error:
		LoggingUtils::getInstance().logToFile(_oss.str(), "ERROR");
		break;
		//abort();
	}
}

