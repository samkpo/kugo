#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include "utils.h"

QString Utils::stringFromFile(const QString &name)
{
  QString ret_string;
  QFile file(name);
  if (file.open(QIODevice::ReadOnly))
  {
      QTextStream ts(&file);
      ts.setCodec("UTF-8");
      ret_string = ts.readAll();
      file.close();
  }
  return ret_string;
}
