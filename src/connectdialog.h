#ifndef CONNECTDIALOG_H
#define CONNECTDIALOG_H

#include <QDialog>
#include "defines.h"

//Let use the serial port
class QSerialPort;

//Used to show the main winows
namespace Ui {
class ConnectDialog;
}


/**
 * @brief      Class used to show a connection dialog, that will configure the
 *             port.
 */
class ConnectDialog : public QDialog
{
    Q_OBJECT
public:
    /*!
     * @brief      Constructor
     *
     * @param      parent  The parent widget
     */
    ConnectDialog(QWidget *parent = 0);

    /**
     * This sets the serial port that we'll connect to
     *
     * @param      port  The port
     */
    void setPortToConnect(QSerialPort *port);

signals:
    /**
     * @brief      Signal emmited if the port is open
     */
    void portOpened(bool);

private:
    /**
     * Dialog ui design
     */
    Ui::ConnectDialog *ui;

    /**
     * Serial port to be manipulated
     */
    QSerialPort* mPort = nullptr;
    QString mPortName;
    qint32 mBaudRate = 19200;

    /**
     * @brief      configures the port list
     */
    void configurePortCombo(QString defPort="");

    /**
     * @brief      Configures the dialog ui, depending on whether the port is
     *             open or closed
     *
     * @param[in]  b port open?
     */
    void connectedButtons(bool b);

private slots:
    /**
     * @brief      slot to listen to the user port selection
     *
     * @param[in]  portName  The port name
     */
    void portChangedSlot(QString portName);

    /**
     * @brief      Connects the connect button to the slot.
     */
    void connectButtonSlot();

    /**
     * @brief      Connects the cancel button to the slot.
     */
    void cancelButtonSlot();

};

#endif // CONNECTDIALOG_H
