#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

#include <QDialog>
#include "ui_aboutdialog.h"

class AboutDialog : public QDialog
{
        Q_OBJECT

    public:
        /*!
          constructor
          */
        AboutDialog(QWidget* parent = 0, Qt::WindowFlags fl = 0);

    private:
        Ui_AboutDialog ui;

};

#endif
