#ifndef UTILS_H
#define UTILS_H

#include <QObject>
#include <QtCore/QStringList>

/**
 * @brief      Class for utilities.
 *
 * This class contains (or will contain) several methods that will be used all
 * across the app.
 */
class Utils : public QObject
{
    Q_OBJECT
public:
    Utils(){};

    /**
     * @brief      Reads a text file \a fileName, and returns it's data
     *
     * @param[in]  fileName  The file name
     *
     * @return     A string containing all the data from the file
     * \todo Make it a list of strings, an element for line
     */
    static QString stringFromFile(const QString &fileName);

private:
  
};

#endif // UTILS_H
