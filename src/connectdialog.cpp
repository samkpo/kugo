#include <QtGui>
#include <QMessageBox>
#include <QSerialPort>
#include <QSerialPortInfo>
#include "cdebug.h"
//#include "serialport.h"
#include "connectdialog.h"
#include "ui_connectdialog.h"

ConnectDialog::ConnectDialog(QWidget* parent) :
    QDialog( parent ),
    ui(new Ui::ConnectDialog)
{
    //Configuramos la interfaz
    ui->setupUi(this);

    // Setting
    QSettings settings;

    // Last port
    auto lastPort = settings.value("lastPort", "").toString();

    // ports combo
    this->configurePortCombo(lastPort);

    //Set the default port
    mPortName = ui->portComboBox->currentText();

    // Baud rate
    mBaudRate = settings.value("baud_rate", 19200).toInt();

#ifndef NDEBUG
    DEBUG << tr("Baud rate: ") << mBaudRate;
#endif

    //Conectamos los slot
    connect(ui->portComboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(portChangedSlot(QString)));
    connect(ui->portComboBox, SIGNAL(currentTextChanged(QString)), this, SLOT(portChangedSlot(QString)));
    connect(ui->connectButton, SIGNAL(pressed()), this, SLOT(connectButtonSlot()));
    connect(ui->cancelButton, SIGNAL(pressed()), this, SLOT(cancelButtonSlot()));
}

void ConnectDialog::cancelButtonSlot()
{
    //Cerramos el dialogo
    this->close();
}

void ConnectDialog::setPortToConnect(QSerialPort *port)
{
    //Guardamos el puerto
    this->mPort = port;
}

void ConnectDialog::connectedButtons(bool b)
{
    if(!b)
        return;

    ui->connectButton->setEnabled(false);
}

void ConnectDialog::connectButtonSlot()
{
    //CHeck that the given port is setted
    if(this->mPort == nullptr){
        DEBUG << "Port not open yet";
        return;
    }

    //Log it
    INFO << tr("Connected to %1 @ %2").arg(this->mPortName).arg(mBaudRate);

    //Nos conectamos
    this->mPort->setPortName(this->mPortName);
    this->mPort->setBaudRate(this->mBaudRate);
    bool result = this->mPort->open(QIODevice::ReadWrite);

    // Setting
    QSettings settings;

    //Si se conecto lo dejamos en claro
    if(result){
        DEBUG << __FUNCTION__ << "Puerto conectado";
        settings.setValue("lastPort", this->mPortName);

        //Configuramos los botones
        this->connectedButtons(true);

        //Emitimos la señal de apertura
        emit(portOpened(true));

        //Cerramos el dialogo
        this->close();
    } else {
        DEBUG << "No se pudo conectar";

        //Not opened
        emit(portOpened(false));

        QMessageBox::warning(this,
                             tr("Kugo"),
                             tr("The bench isn't connected, or the port is not the right one"),
                             QMessageBox::Ok);
    }
}

void ConnectDialog::configurePortCombo(QString defPort)
{
    QStringList ports;
    for(auto &i: QSerialPortInfo::availablePorts())
        ports << i.portName();

    ui->portComboBox->addItems(ports);
    if(not defPort.isEmpty())
        ui->portComboBox->setCurrentIndex(ports.indexOf(defPort));
}

void ConnectDialog::portChangedSlot(QString newPort)
{
    this->mPortName = newPort;
    DEBUG << __FUNCTION__ << "New port: " << newPort;
}

