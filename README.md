# Kugo project

## TODO list
- Time checking and setting
- Connection check on open port
- Connection checking while open
- Dialog o method to set the logging file
- Finish user logging stuff


## Connection check between Kugo and the bench
There's obviously a need to check that we are actually talking to the work bench and not to any other device connected to any serial port, so we need a way to do so. That checking must be done while the program opens the connection.

The protocol is the following:

*   Open port
*   Send keyword (connection_word)
*   Wait for response. It will be like this:
    <CONNECTION_WORD>_OK
*   After that we can conclude that the port has been successfully open.

## Connection status check
Another thing to check is if the serial connection is still open, so the bench doesn't goes on it's own.

One way to check the connection status is that bench and program send data, and the other responds.

Let's say that the bench send the current reading, and stores a checking word formed with that data. Then, when the program receives the reading makes the same process, and send it back to the bench, it check the received word, and if it's correct keeps working, and if after a certain number of errors it doesn't coincide, it stops working.

## Loads list generation
Using the conma app:

```
./conma -f val.txt -sbPn | sed '1,2d' | sed 's/        /    {/' | sed 's/\t     /, /' | sed 's/\t0b/ 0b/' | sed 's/ 0b/, 0b/g' | sed 's/$/},/'
```

### Logging stuff
Before going to the logging details, we must set some background rules of what is going to be logged to the log file and what is not.

The log file must contain only data that will help diagnose failures in case of malfunction, or a wrong usage of the system, so it needs to contain data that helps into that task, but not too much so it's unreadable.

### Available options

In the sources there will three logging options:
 
 * Info - This outputs the desired data to the logging file (talk about this later). 
 * Warning - Same as info, but should be used when things require more attention.
 * Debug - This outputs data to the command line where you called the app from (if so) and, if the necessary flag is on, to the logging file as well.
 * Reading - this is used to log every reading from the bench.
 * Error - Same as Info

All the three of them will output the data in a predefined format:

```
[DD/MM/YY hh:mm:ss.zzz USER LABEL]: message
```

Where _**USER**_ is the one logged in the program, and _**LABEL**_ may be one of three mentioned above.

With that information is up to the coder to decide which option is the one that fits in each part of the app.

### Usage

To call any of the mentioned options, the user just needs to include the necessary header, and call the macro:

```
#include "cdebug.h"
..

DEBUG << "debugging";
LOG << "logging";
WARNINGN << "caution with this"
ERROR << "erroring?";
```

### What to log

The point of all this is what are we going to log? And that's a valid question, here are some items that are a must to:

*   When user logs in
*   When the app connects to the board
*   When there is a change on the configuration, like:
    -   Wye to star, and conversely
    -   Equilibrated to desequilibrated
    -   The resistance value of any of the branches
*   Connection/Disconnection of the bench to the load/source
*   Every value read by the meter.


## Data transmission

When connecting